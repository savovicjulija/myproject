package com.in28minutes.springboot.rest.example.unittest;

import com.in28minutes.springboot.rest.example.entity.Student;
import com.in28minutes.springboot.rest.example.repository.StudentRepository;
import com.in28minutes.springboot.rest.example.service.StudentServiceImplem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
public class StudentServiceMockTest {

    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentServiceImplem studentService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllStudents() {
        //prepare
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(new Student(100L, "Marry", "A546788"));
        studentList.add(new Student(101L, "Maraya", "O987678"));
        studentList.add(new Student(102L, "Zack", "P234567"));
        studentList.add(new Student(103L, "Jim", "I989088"));
        when(studentRepository.findAll()).thenReturn(studentList);

        //exercise
        List<Student> result = studentService.getAllStudents();

        //verify
        assertEquals(4, result.size());
    }

    @Test
    public void testGetStudentById() {
        //prepare
        Student student = new Student(200L, "Jim", "L234567");
        when(studentRepository.findById(200L)).thenReturn(Optional.ofNullable(student));

        //exercise
        Optional<Student> result = studentService.getStudentById(200L);

        //verify
        //assertEquals(200L, Optional.ofNullable(student.getId()));
        assertEquals("Jim", student.getName());
        assertEquals("L234567", student.getPassportNumber());
    }

    @Test
    public void create() {
        //prepare
        Student student = new Student(15L, "Layla", "P908976");
        when(studentRepository.save(student)).thenReturn(student);

        //exercise
        Optional<Student> result = studentService.create(student);
        Student student1 = result.get();

        //verify
        assertEquals(student.getId(), student1.getId());
        assertEquals(student.getName(), student1.getName());
        assertEquals(student.getPassportNumber(), student1.getPassportNumber());
    }

    @Test(expected = RuntimeException.class)
    public void createIfNull() {
        //prepare
        Student student = null;
        when(studentRepository.save(student)).thenReturn(student);

        //exercise
        Optional<Student> result = studentService.create(student);
    }

    @Test
    public void deleteStudentById() {
        //prepare
        Student student = new Student(300L, "Micky", "B342536");
        when(studentRepository.findById(300L)).thenReturn(Optional.of(student));

        //exercise
        studentService.deleteStudentById(300L);

        //verify
        verify(studentRepository, times(1)).deleteById(300L);

    }

   @Test
    public void updateStudent() {
      //prepare
        Student student = new Student(500L, "Nick", "U789098");
        Student student2 = new Student(501L, "Nick", "M7654321");
        when(studentRepository.findById(501L)).thenReturn(Optional.of(student));

        //exercise
        Optional<Student> result = studentService.update(student, 501L);

        Student student1 = result.get();

        //verify
        assertEquals(student1.getId(), student.getId());
        assertEquals(student1.getName(), student.getName());
        assertEquals(student1.getPassportNumber(), student.getPassportNumber());

    }

}

