package com.in28minutes.springboot.rest.example.service;

import com.in28minutes.springboot.rest.example.entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    List<Student> getAllStudents();

    Optional<Student> getStudentById(Long id);

    Optional<Student> create(Student student);

    Optional<Student> update(Student student, long id);

    void deleteStudentById(Long id);
}

