package com.in28minutes.springboot.rest.example.service;


import com.in28minutes.springboot.rest.example.entity.Student;
import com.in28minutes.springboot.rest.example.exception.StudentNotFoundException;
import com.in28minutes.springboot.rest.example.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service("studentService")
@Transactional

public class StudentServiceImplem implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    Optional<Student> studentOptional;

    @Override
    public List<Student> getAllStudents() {
        List<Student> students = studentRepository.findAll();
        return students;
    }

    @Override
    public Optional<Student> getStudentById(Long id) {
        studentOptional = studentRepository.findById(id);
        if (!studentOptional.isPresent()) {
            throw new StudentNotFoundException("Student doesn't exist!");
        }
        return studentOptional;
    }

    @Override
    public Optional<Student> create(Student student) {
        if(student==null){
            throw new RuntimeException("Student je null");
        }
        Student s = studentRepository.save(student);
        return Optional.of(s);
    }

    @Override
    public Optional<Student> update(Student student, long id) {
        studentOptional = studentRepository.findById(id);
        if (!studentOptional.isPresent()) {
            throw new StudentNotFoundException("Student doesn't exist!");
        }
        student.setId(id);
        studentRepository.save(student);
        return Optional.of(student);

    }

    @Override
    public void deleteStudentById(Long id) {
        studentOptional = studentRepository.findById(id);
        if (!studentOptional.isPresent()) {
            throw new StudentNotFoundException("Student doesn't exist!");
        }
        studentRepository.deleteById(id);

    }

}
