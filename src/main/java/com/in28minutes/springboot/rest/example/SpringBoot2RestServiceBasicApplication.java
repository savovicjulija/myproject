package com.in28minutes.springboot.rest.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EntityScan( basePackages = {"com.in28minutes.springboot.rest.example"})

@SpringBootApplication

public class SpringBoot2RestServiceBasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot2RestServiceBasicApplication.class, args);
    }
}
